/**
 * Employee interface
 * @author Jackson Kwan
 */
package jacksonkwantest2q2;

public interface Employee {
	double getWeeklyPay();
}
