/**
 * Weekly pay calculated with yearly salary
 * @author Jackson Kwan
 */
package jacksonkwantest2q2;

public class SalariedEmployee implements Employee{
	private double yearlySalary;
	
	/**
	 * parameterized constructor
	 * @param yearlySalary yearly salary of the employee
	 */
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	/**
	 * calculates the weekly salary with yearly pay
	 */
	public double getWeeklyPay() {
		return yearlySalary/52;
	}
	/**
	 * getter for yearlysalary
	 * @return yearlySalary
	 */
	public double getYearlySalary() {
		return yearlySalary;
	}
	
}
